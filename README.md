# Google is Danger

By Ade Malsasa Akbar.

Do you know the danger of Google? Did you even wonder from where Google make money? This is the danger: Google receives money by selling your data, after collecting them every single time, to companies across all countries for advertising purposes. You should beware of Google and don't think you don't pay for their comfortability. Also, don't let your admiration to them deceive you.  If this surprises you, please continue reading. 

# Google Knows All About You

Visit  https://myactivity.google.com. Beware, this might scare you. You will see what Google know about you and surely you will say it's just too much. Why did you see so many information about your life on that web page? It's because Google deliberately collects every single information, every single second, every day, when you use their services like Google Search, Gmail and YouTube. 

# Google is not Gratis

They have many online services. The biggest of them all are Google Search, Gmail and YouTube. Think about it, why did you always think these products free? It's simply because you didn't know you are the product, meaning, Google collects all your information as long as you use their services, then sell those information to companies for money. Google services are not gratis, because you pay with your privacy, personal data and private life.

# Privacy-Disrespecting

To make discussion easier, we should name things, and for this issue with Google, we can name this issue "privacy-disrespecting".

# The Issues

Now we learn the actual issues by examples we picked here the biggest one. This does not mean the unmentioned ones do not have issues.

- Gmail reads your emails, all of them, both inbox and outbox.
- Google Search records history of your IP address, physical location, web browser, what you search, what you click, when, how long, how frequent, until they learned things you like and dislike.
- YouTube records all of your activities, videos you search, click, view, like and dislike, for how long.
- Google Docs also records everything, and runs proprietary software (=software that does not respects user's freedom) in your computer through your browser. It is an [SaaSS, an issue worse than even Microsoft Office](https://www.gnu.org/philosophy/who-does-that-server-really-serve.en.html), according to the Free Software Community.
- AdSense gives you money, but Google harvests other people's data and sell them first to do so.
- AdWords is Google's source of money: companies pay Google, then Google displays ads to us, then Google receives money, we lost our privacy.
- Analytics helps you chart your visitors, but Google disrespects people's privacy to do so. 
- Millions of other websites cooperated with Google in form of displaying AdSense and YouTube to us.

# Android is Ads Conveyor

People worldwide started to love Google since the mass production of phones with Google's operating system, Android. Google led people to think they are good, cool, technologically advanced, comfort us, helping us generously with this greet robot OS. Think about it, why did you see ads on your phone, even in every app and since when? Why could the ads precisely suit what you like and dislike? That's a sign that Google spies on you through Android. Before Android, Google could only spies on you on web browser and emails, but after it Google can also spies you through your phone. Now you understand that YouTube App, Play Store, Chrome within Android purposes are not only limited to what their names appear to be, right? 

# Chrome is Google's Trojan

The Free Software Community [reported that Google Chrome browser is spyware with evidences](https://www.gnu.org/proprietary/malware-google.en.html) since long time ago. Chrome records what you visit, even what you type, everyday, and send that information to Google without you knowing. Chrome is a proprietary software (=software that the user is fully controlled by the developer) so we cannot believe anything it claims because they provide no proof. Think about it, since when people worldwide love Chrome? It's since the mass production of phones with Android OS. Think again, why? Because Chrome is Google's recorder and listening device againsts the users, it enhances Google business to record users more and sell the data more for more money. If you do not use Chrome, Google can only spy when you visit their website, but with Chrome, Google can spy on you even when you do not visit anything. Thanks to Chrome, Google led you to Google products, and only Google's. Now you do not know Yahoo!, AOL, Firefox and other alternatives, right? You should consider to use no Chrome in your phone, computer, and all other devices. 

# People Who Admire Google Blindly

It is also a serious issue that many people admire Google blindly. Mostly, it's because of Google's ability to spread out their company's opinions, their advancement in technology and computing, while many people never heard that Google sell out their personal data. Be careful, these blind admirers of people  will attract you into Google. You should have some ability to educate them the the urgency to switch away from Google. 

# Switching Away From Google 

See Alternatives section to switch away from Google. In other words, to do computing with internet without Google. Please keep in mind that this is not easy, especially hard for Gmail part, as there's very a little number of privacy respecting AND gratis email service providers, otherwise you indeed should look for the PAID ones. Don't think paid is equal to bad or unethical, but privacy disrespecting is. 

# Solutions

The solutions to Google privacy-disrespecting issue are a few, otherwise you're capable to do self-hosting, you seek for alternatives.  This means otherwise you create your own alternatives in your own computing, you use alternatives made available by someone else. For Google Chrome browser, you can use Mozilla Firefox instead, a free software (=software that is fully controlled by the user, not the developer). Special for Gmail, if you for whatever reason cannot leave Gmail, you need to learn to encrypt emails (GnuPG is the most famous solution). 

# Web Browser
The easiest switching is to change your Chrome browser into another. You can use Mozilla Firefox, the free software browser (=sofware that respects user's freedom) that is famous, was number one before Android phones mass productions.

# Self-Hosting 

Don't do this if you do not have enough computing knowledge or facility, but see Alternatives section instead. Otherwise you can consider these technologies and do server self-hosting by yourself:

Yunohost:
    The operating system to build a complete self-hosting system almost as complete as Google.

Searx:
    Software to create your own Google Search.

Iredmail:
    Software to create your own Gmail Server.

PeerTube:
    Software to create your own YouTube.

WordPress:
    Software to create your own Blogger.com.

OnlyOffice Online:
    Software to create your own Google Docs.

Nextcloud: 
    Software to create your own Google Drive and others almost as complete as Google Suite. 

Jitsi Meet:
    Software to create your own Google Meet.

# Alternatives

BEWARE: DON'T THINK THAT EVERY SERVICE WILL BE THE SAME OVER TIME, AS THEY MAY CHANGE AT ANY TIME!

You can use these  online services as an alternative to Google's (otherwise do self-hosting if you're capable to):

Startpage:
        To search.

Invidious and PeerTube:
        To watch YouTube videos. (*)
        To completely switch away from YouTube with your society.

Mailo:
        To replace Gmail free-of-cost. (*)

WordPress.com:
        To replace Blogspot.com.

CryptPad: 
        To replace Google Forms. 

# More Alternatives and Self-Hosting Solutions

BEWARE: DON'T THINK THAT EVERY SERVICE WILL BE THE SAME OVER TIME, AS THEY MAY CHANGE AT ANY TIME!

There are surely many other alternatives. Here's some of them:

For  Search:
- DuckDuckGo
- Searx 

For Gmail:
- Disroot
- Protonmail
- Tutanota

For Drive: 
- Nextcloud Services 
- Internet Archive

For Google Chrome browser:
- Icecat (from GNU Operating System)
- Abrowser (built-in Trisquel GNU/Linux)
- Ungoogled
- Iridium
- Tor Browser

# Encryption

Security experts always encourage and many of them even mandate encryption. For emails, because most of people today rely on Gmail (a free-of-cost but privacy-disrepecting service), and chances are that you also cannot switch away from it, then you should consider to learn to encrypt your emails. To do so, they mileage is like below:
1. Learn not to use Gmail from web browser or the official application.
2. Learn to use email client program instead. For example, Thunderbird and K-9 Mail. 
3. Learn to use GnuPG encryption in the email client. For example, in Thunderbird people use Enigmail tool to make it easy since a long time.
4. Ask kindly, politely your email friends to use the same encryption like yours. 